#include "cs225/PNG.h"
#include "cs225/HSLAPixel.h"
#include <string>
#include "mp1.h"
#include <iostream>
#include <cstdlib>
#include <cmath>

using namespace cs225;


void rotate(std::string inputFile, std::string outputFile) {
  PNG* original = new PNG;
  original->readFromFile(inputFile);
  unsigned w = original->width();
  unsigned h = original->height();
  PNG* output = new PNG(w,h);

  for (unsigned  y = 0;  y < h; y++) {
    for (unsigned  x = 0 ;  x < w; x++){
      HSLAPixel & prevpic = original->getPixel(w-x-1, h-y-1);
      HSLAPixel & newpic = output->getPixel(x,y);

      newpic.h = prevpic.h;
      newpic.s = prevpic.s;
      newpic.l = prevpic.l;
      newpic.a = prevpic.a;

    }
  }
  output->writeToFile(outputFile);

}

PNG myArt(unsigned int width, unsigned int height) {
  PNG png(width, height);
  for (unsigned  y = 0;  y < height; y++) {
    for (unsigned  x = 0 ;  x < width; x++){
      HSLAPixel & randompic = png.getPixel(x,y);

      randompic.h = 360*(sqrt(((x-200)*(x-200))+((y-200)*(y-200)))/sqrt((height*height)+(width*width)));
      randompic.s = 1*(sqrt(((x-200)*(x-200))+((y-200)*(y-200)))/sqrt((height*height)+(width*width)));
      randompic.l = 1-(1*(sqrt(((x-200)*(x-200))+((y-200)*(y-200)))/sqrt((height*height)+(width*width))));
    }
  }

  return png;
}
