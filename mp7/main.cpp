#include <iostream>
#include "dsets.h"
#include "maze.h"
#include "cs225/PNG.h"

using namespace std;

int main()
{
    // Write your own main here
    SquareMaze m;
    m.makeMaze(20, 50);
    PNG *maze = m.drawMazeWithSolutionColor();
    maze->writeToFile("creative.png");
    delete maze;
    return 0;
}
