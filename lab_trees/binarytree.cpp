/**
 * @file binarytree.cpp
 * Definitions of the binary tree functions you'll be writing for this lab.
 * You'll need to modify this file.
 */
#include "TreeTraversals/InorderTraversal.h"
#include <iostream>
#include <stack>
using namespace std;
/**
 * @return The height of the binary tree. Recall that the height of a binary
 *  tree is just the length of the longest path from the root to a leaf, and
 *  that the height of an empty tree is -1.
 */
template <typename T>
int BinaryTree<T>::height() const
{
    // Call recursive helper function on root
    return height(root);
}

/**
 * Private helper function for the public height function.
 * @param subRoot
 * @return The height of the subtree
 */
template <typename T>
int BinaryTree<T>::height(const Node* subRoot) const
{
    // Base case
    if (subRoot == NULL)
        return -1;

    // Recursive definition
    return 1 + max(height(subRoot->left), height(subRoot->right));
}

/**
 * Prints out the values of the nodes of a binary tree in order.
 * That is, everything to the left of a node will be printed out before that
 * node itself, and everything to the right of a node will be printed out after
 * that node.
 */
template <typename T>
void BinaryTree<T>::printLeftToRight() const
{
    // Call recursive helper function on the root
    printLeftToRight(root);

    // Finish the line
    cout << endl;
}

/**
 * Private helper function for the public printLeftToRight function.
 * @param subRoot
 */
template <typename T>
void BinaryTree<T>::printLeftToRight(const Node* subRoot) const
{
    // Base case - null node
    if (subRoot == NULL)
        return;

    // Print left subtree
    printLeftToRight(subRoot->left);

    // Print this node
    cout << subRoot->elem << ' ';

    // Print right subtree
    printLeftToRight(subRoot->right);
}

/**
 * Flips the tree over a vertical axis, modifying the tree itself
 *  (not creating a flipped copy).
 */
template <typename T>
void BinaryTree<T>::mirror()
{
    mirror(root);
}

template <typename T>
void BinaryTree<T>::mirror(Node* node)
{
    if(node == NULL)
      return;
    Node* temp = node->left;
    node->left = node->right;
    node->right = temp;
    mirror(node->left);
    mirror(node->right);
}


/**
 * isOrdered() function iterative version
 * @return True if an in-order traversal of the tree would produce a
 *  nondecreasing list output values, and false otherwise. This is also the
 *  criterion for a binary tree to be a binary search tree.
 */
template <typename T>
bool BinaryTree<T>::isOrderedIterative() const
{
  if(root->right == NULL && root->left == NULL)
    return true;
  int h = height(root);
  stack<typename BinaryTree<T>::Node*> stack;
  int size = 1;
  for (int i = 0; i < h; i++) {
    if(i == 0)
      size = 1;
    else{
      for(int j = i; j != 0; j--){
        size+=2;
      }
    }
  }

  vector<int> v;
  v.resize(size);
  int popcount = 0;
  Node* curr = root;
label:
  while (curr != NULL) {
    stack.push(curr);
    curr = curr->left;
  }
  if(curr == NULL && !stack.empty()){
    v[popcount] = stack.top()->elem;
    curr = stack.top()->right;
    stack.pop();
    popcount++;
    goto label;
  }
  if (curr == NULL && stack.empty()) {
    for (int i = 0; i < (popcount - 1); i++) {
      if(v[i] >= v[i+1])
        return false;
    }
    return true;
  }
  return false;
}


/**
 * isOrdered() function recursive version
 * @return True if an in-order traversal of the tree would produce a
 *  nondecreasing list output values, and false otherwise. This is also the
 *  criterion for a binary tree to be a binary search tree.
 */
template <typename T>
bool BinaryTree<T>::isOrderedRecursive() const
{
    return isOrderedRecursive(root);
}

template <typename T>
bool BinaryTree<T>::isOrderedRecursive(const Node* node) const
{
  if(node->left == NULL && node->right == NULL)
    return true;
  if(node->left == NULL)
    return ((isOrderedRecursive(node->right))&&(node->elem < node->right->elem));
  if(node->right == NULL)
    return ((isOrderedRecursive(node->left))&&(node->elem > node->left->elem));
  if(isOrderedRecursive(node->left)&&isOrderedRecursive(node->right)){
    if((node->elem > node->left->elem)&&(node->elem < node->right->elem)){
      return true;
    }
  }
  return false;
}

/**
 * creates vectors of all the possible paths from the root of the tree to any leaf
 * node and adds it to another vector.
 * Path is, all sequences starting at the root node and continuing
 * downwards, ending at a leaf node. Paths ending in a left node should be
 * added before paths ending in a node further to the right.
 * @param paths vector of vectors that contains path of nodes
 */
template <typename T>
void BinaryTree<T>::printPaths(vector<vector<T> > &paths) const
{
    vector<int> v{};
    printPaths(root, paths, v);
}
template <typename T>
void BinaryTree<T>::printPaths(const Node* node, vector<vector<T> > &paths, vector<int> v) const
{
    v.push_back(node->elem);
    if(node->left == NULL && node->right == NULL){
      paths.push_back(v);
    }
    if(node->left != NULL){
      printPaths(node->left, paths, v);
    }
    if(node->right != NULL){
      printPaths(node->right, paths, v);
    }
    v.pop_back();
    return;
}

/**
 * Each node in a tree has a distance from the root node - the depth of that
 * node, or the number of edges along the path from that node to the root. This
 * function returns the sum of the distances of all nodes to the root node (the
 * sum of the depths of all the nodes). Your solution should take O(n) time,
 * where n is the number of nodes in the tree.
 * @return The sum of the distances of all nodes to the root
 */
template <typename T>
int BinaryTree<T>::sumDistances() const
{
  int lvl = 0;
  return sum(root, lvl);
}
template <typename T>
int BinaryTree<T>::sum(Node* node, int lvl) const
{
  int left = 0;
  int right = 0;
  if(node == NULL)
    return 0;
  left = sum(node->left, lvl +1);
  right = sum(node->right, lvl+1);
  return lvl + left + right;
}
