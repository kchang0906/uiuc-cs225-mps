#include <cmath>
#include <iterator>
#include <iostream>

#include "../cs225/HSLAPixel.h"
#include "../cs225/PNG.h"
#include "../Point.h"

#include "ImageTraversal.h"

/**
 * Calculates a metric for the difference between two pixels, used to
 * calculate if a pixel is within a tolerance.
 *
 * @param p1 First pixel
 * @param p2 Second pixel
 * @return the difference between two HSLAPixels
 */

double ImageTraversal::calculateDelta(const HSLAPixel & p1, const HSLAPixel & p2) {
  double h = fabs(p1.h - p2.h);
  double s = p1.s - p2.s;
  double l = p1.l - p2.l;

  // Handle the case where we found the bigger angle between two hues:
  if (h > 180) { h = 360 - h; }
  h /= 360;

  return sqrt( (h*h) + (s*s) + (l*l) );
}
/**
 * Default iterator constructor.
 */
ImageTraversal::Iterator::Iterator() {
  /** @todo [Part 1] */
  t_ = NULL;
}

ImageTraversal::Iterator::Iterator(ImageTraversal* t) {
  /** @todo [Part 1] */
  t_ = t;
  start_ = t_->getStart();
  image_ = t_->getImage();
  tolerance_ = t_->getTolerance();
  w_ = image_.width();
  h_ = image_.height();
  visited_ = new int*[w_];
  for (int i = 0; i < w_; i++) {
    visited_[i] = new int[h_];
  }
  for (int i = 0; i < w_; i++) {
    for (int j = 0; j < h_; j++) {
      visited_[i][j] = 0;
    }
  }
  t_->add(start_);

}

/**
 * Iterator increment opreator.
 *
 * Advances the traversal of the image.
 */
ImageTraversal::Iterator & ImageTraversal::Iterator::operator++() {
  /** @todo [Part 1] */
  Point pop = t_->pop();
  int tempX = pop.x;
  int tempY = pop.y;
  visited_[tempX][tempY] = 1;
  int x = start_.x;
  int y = start_.y;
  HSLAPixel startPoint = image_.getPixel(x,y);

  if(tempX+1 < w_){
    if(visited_[tempX+1][tempY] != 1){
      HSLAPixel temp = image_.getPixel(tempX+1, tempY);
      double t = calculateDelta(startPoint, temp);
      if(t <= tolerance_){
        Point* p = new Point(tempX+1, tempY);
        t_->add(*p);
      }
    }
  }

  if(tempY+1 < h_){
    if(visited_[tempX][tempY+1] != 1){
      HSLAPixel temp = image_.getPixel(tempX, tempY+1);
      double t = calculateDelta(startPoint, temp);
      if(t <= tolerance_){
        Point* p = new Point(tempX, tempY+1);
        t_->add(*p);
      }
    }
  }

  if(tempX-1 >= 0){
    if(visited_[tempX-1][tempY] != 1){
      HSLAPixel temp = image_.getPixel(tempX-1, tempY);
      double t = calculateDelta(startPoint, temp);
      if(t <= tolerance_){
        Point* p = new Point(tempX-1, tempY);
        t_->add(*p);
      }
    }
  }

  if(tempY-1 >= 0){
    if(visited_[tempX][tempY-1] != 1){
      HSLAPixel temp = image_.getPixel(tempX, tempY-1);
      double t = calculateDelta(startPoint, temp);
      if(t <= tolerance_){
        Point* p = new Point(tempX, tempY-1);
        t_->add(*p);
      }
    }
  }

  Point p = t_->peek();
  int newX = p.x;
  int newY = p.y;
  while(visited_[newX][newY] == 1 && t_->empty() == false){
    p = t_->pop();
    if(t_->empty() == false){
      p = t_->peek();
      newX = p.x;
      newY = p.y;
    }
  }
  return *this;
}

/**
 * Iterator accessor opreator.
 *
 * Accesses the current Point in the ImageTraversal.
 */
Point ImageTraversal::Iterator::operator*() {
  /** @todo [Part 1] */
  return t_->peek();
}

/**
 * Iterator inequality operator.
 *
 * Determines if two iterators are not equal.
 */
bool ImageTraversal::Iterator::operator!=(const ImageTraversal::Iterator &other) {
  /** @todo [Part 1] */
  return !(t_->empty());
}
